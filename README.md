# COVID-19 Resources

a compilation of helpful resources which have received good feedback and validation and seem to be reliable, reproduce-able with proper information, research and support available.

PPE = Personal Protective Equipment
Websites with Reliable Data / Medically Approved Designs

## GENERIC PPE
	http://www.makeppe.net/
	Efforts being ARCHIVED on one site for PPE production
	
	https://www.providence.org/lp/100m-masks
	100 million mask challenge by Providence 
	FILES
		https://psjh.blob.core.windows.net/covid/PSJH_Faceshield.pdf
		https://psjh.blob.core.windows.net/covid/2020-%20PSJH_CLINICALFACEMASK_%20Instructions%20-%20Final.pdf
		
	https://kaastailored.com/ppe-at-kaas/
	PPE GUIDELINE for INDUSTRIAL PRODUCERS
	
	https://docs.google.com/spreadsheets/u/0/d/1e81ceSrVT6Bl4UaS1SxIUUUzgZAdkIstU7Hzot08Ld8/htmlview?
	Google Sheet with Effort to Bring MANY DESIGNS in one place


## DIY PPE Efforts
	
	https://drive.google.com/file/d/1JeMvp10fJk-0MRF72bZmfhuTc_1vDekt/view?
	FACE SHIELD

	University of Hong Kong – Shenzhen Hospital
	recommends this DIY Mask and Shield
	https://www.youtube.com/watch?v=JY-29VBkGmw&feature=youtu.be
	https://www.consumer.org.hk/ws_en/news/2020/covid-19-diymasks
	https://www.consumer.org.hk/ws_en/news/specials/2020/mask-diy-tips.html
	

	Online Links for Face Mask / 3D Printed - could be ealisy made with injection molding
	Facebook Post Link: https://www.facebook.com/groups/opensourcecovid19medicalsupplies/permalink/677025009774561/
	3D Model Links
	1. https://grabcad.com/library/diy-face-mask-1
	2. https://www.thingiverse.com/thing:4231792
	
## PPE - FACE MASK
	https://makermask.org/
	Make masks at home, with a sewing machine, requires Non-woven polypropylene (NWPP) Bags as the main material


## PPE - FACE SHIELDS
	https://www.prusa3d.com/covid19/
	Prusa3D Covid-19 Efforts Main page

	https://www.prusaprinters.org/prints/25857-prusa-protective-face-shield-rc3
	Prusa Protective Face Shield - Release Candidate 3
	Tested and being used by Gov/Hospitals in Czech
	Needs 3D Printer and a Transparent plastic sheet - used in file fodlers

	https://dimin.com/face-shield/?nab=0&utm_referrer=https%3A%2F%2Fwww.facebook.com%2F
	FACE SHIELD using simple plastic sheets Die-Cut


## PPE - BODY GOWN
	https://www.facebook.com/michdulce/posts/10163041065735705
	Medically Approved Gown
	Google Drive Link: https://drive.google.com/drive/folders/1-Xr-67gs2qw0o44-hy-INb89I9m-wcLD?

	https://drive.google.com/drive/folders/1BXudChWB3_Aqzs2hmL1x3h2rnplGItxa?

	NOT YET VERIFIED MEDICALLY - but made by a professional
	https://www.dropbox.com/sh/4ixh31ino8y3qcx/AADtrrJrU5o8oBr8SBo4zXh2a/MedicalPatternZaborneyPDF?dl=0&subfolder_nav_tracking=1
	https://drive.google.com/drive/folders/1BXudChWB3_Aqzs2hmL1x3h2rnplGItxa?

## VENTILATOR RELATED EFFORTS
	http://ventsplitter.org/
	Ventilator Splitter Valve Designs

